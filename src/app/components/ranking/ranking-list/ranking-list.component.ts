import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms'
import {BackapiService} from '../../../services/backapi/backapi.service'
import {SessionService} from '../../../services/session/session.service'
import score from 'string-score'

@Component({
  selector: 'app-ranking-list',
  templateUrl: './ranking-list.component.html',
  styleUrls: ['./ranking-list.component.css']
})
export class RankingListComponent implements OnInit {

  //Forms.
  public option = new FormControl(0)
  public search = new FormControl('')

  //The original rankding data, eg. the data we filter from.
  private originalRankingData: Array<any> = []
  public rankingData: Array<any> = []
 
  constructor(private backApi: BackapiService,
    private session: SessionService) { }

  /**
   * We fetch the ranking data and sets our originalranking data to the data recieve.
   */
  async ngOnInit() {
    this.originalRankingData = await this.backApi.getRankingList()
    this.rankingData = [...this.originalRankingData]

    //Subscribes to changes in our input.
    this.option.valueChanges.subscribe((value) => {
      this.changeOption(value)
      this.authCheck()
    })

    this.search.valueChanges.subscribe((name) => {
      this.searchFunction(name)
      this.authCheck()
    })
    this.authCheck()
  }
  
  //If we are not authenticated we only want to show top 10.
  authCheck() {
    if(!this.isAuthenticated()) {
      this.rankingData = this.rankingData.slice(0,10)
    }
  }
  /**
   * Manipulates our rankding data based on which option is picked.
   * @param value 
   */
  changeOption(value) {
    value = Number(value)
    if(value === 0) {
      this.rankingData = [...this.originalRankingData]
    }
    else {
      this.rankingData = this.originalRankingData.sort((userA: any, userB: any) => {
        let dataType = value === 1 ? 'uniques' : value === 2 ? 'shiny' : 'spottedTotal'  
        return userB[dataType] - userA[dataType]
      })
    }
    if(this.search.value !== "") {
      this.searchFunction(this.search.value)
    }
  }

  
  searchFunction(name: string) {
    if(name === '') {
      this.changeOption(this.option.value)
    }
    else {
      this.rankingData.sort((userA: any,userB: any) => score(name.toLowerCase(), userB.username.toLowerCase(), 0.5) - score(name.toLowerCase(), userA.username.toLowerCase(), 0.5))
    }
  }
  isAuthenticated() {
    return this.session.get()
  }

}