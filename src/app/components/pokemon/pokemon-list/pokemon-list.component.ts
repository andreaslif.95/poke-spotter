import { Component, OnInit, Input } from '@angular/core';
import { PokeapiService } from '../../../services/pokeapi/pokeapi.service'
import {SessionService} from '../../../services/session/session.service'
import { BackapiService } from '../../../services/backapi/backapi.service'
import { FormControl } from '@angular/forms'
import score from 'string-score'

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.css']
})
export class PokemonListComponent implements OnInit {

  // Formcontrol for user input.
  public option = new FormControl(0)
  public rare = new FormControl(false)

  //To display a loading icon when loading data.
  public loading = true

  //Form controll for searchbar
  public search = new FormControl('')

  constructor(
    private pokeApi: PokeapiService,
    private backApi: BackapiService,
    private session: SessionService ) { }
    
  //Holds data for all pokemons, this array is what is manipulated and the results are stored in pokemons.
  private allPokemons: Array<any> = []

  //Holds manipulated data.
  public pokemons: Array<any> = []


  async ngOnInit() {

    const pokes = await this.pokeApi.getAllPokemonNameAndId()

    //We only want to fetch spotted data if the user is authenticated.
    const spotted = this.isAuthenticated() ? await this.backApi.getSpottedPokemons() : []

    const rarest = await this.backApi.getRarestPokemons()

    // Here we manipulate the pokemon data to display wether the user has seen it, if it was a shiny and if it was a male/female.

    pokes.forEach(poke => {
      poke.spotted = null
      poke.rare = false
      if(spotted[poke.name]) {
        poke.spotted = spotted[poke.name]
      }
      if(rarest.includes(poke.id)) {
        poke.rare = true
      }
    });
    this.allPokemons = pokes
    this.pokemons = pokes

    //We subscribe to changes in the different input feels.
    this.option.valueChanges.subscribe((v) => this.optionChange(v))

    this.rare.valueChanges.subscribe((v) => this.doShowRare(v))

    this.search.valueChanges.subscribe((name) => {
      this.searchFunction(name)
    })
    //If the user isn't authenticated we only show rare pokemons.
    if(!this.isAuthenticated()) {
      this.doShowRare(true)
    }
    this.loading = false
  }

  /**
   * If the cond is true we display only the rare pokemons in the current filtering, otherwise we display all pokemons in the current filtering.
   * @param cond 
   */
  doShowRare(cond: boolean) {
    this.loading = true
    if(cond) {
      this.pokemons = this.pokemons.filter(poke => poke.rare)
      //If we searched for a specific pokemon when we apply this filter it should still filter by the name.
      this.searchFunction(this.search.value)
    }
    else {
      this.optionChange(this.option.value)
    }
    this.loading = false
  }
  /**
   * Here we filter the pokemons based on the decisions listed below
   * Value = 0 -> show all pokemons
   * value = 1 -> show only spotted pokemons
   * value = 2 -> show only unspotted pokemon
   * 
   * We also apply rare/ search filtering to the filtering done. 
   * @param value 
   */
  optionChange(value) {
    this.loading = true
    value = Number(value)
    if(value === 0) {
      this.pokemons = this.allPokemons
    }
    else {
      this.pokemons = this.allPokemons.filter((poke) => (value === 1 ? poke.spotted !== null : poke.spotted === null) )
    }
    if(this.rare.value) {
      this.doShowRare(true)
    }
    this.searchFunction(this.search.value)
    this.loading =false
  }
  /**
   * Sorts the current pokemons based on a name, we display the highest score on top and the lowest at the bottom.
   * @param name - Searched name
   */
  searchFunction(name: string) {
    this.loading = true
    if(name === '') {
      this.pokemons.sort((pokeA: any,pokeB: any) => pokeA.id - pokeB.id)
    }
    else {
      this.pokemons.sort((pokeA: any,pokeB: any) => score(name.toLowerCase(), pokeB.name.toLowerCase(), 0.5) - score(name.toLowerCase(), pokeA.name.toLowerCase(), 0.5))
    }
    this.loading = false
  }

  isAuthenticated() {
    return this.session.get()
  }
}
