import { Component, OnInit, Input } from '@angular/core';
import {PokeapiService} from '../../../services/pokeapi/pokeapi.service'
import { faMars, faVenus, faMagic, faEye, faRegistered } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'app-pokemon-list-item',
  templateUrl: './pokemon-list-item.component.html',
  styleUrls: ['./pokemon-list-item.component.css']
})
/**
 * This component only displays information about a specific pokemon.
 */
export class PokemonListItemComponent implements OnInit {

  constructor(private pokeApi: PokeapiService) { }

  public faMars = faMars
  public faVenus = faVenus
  public faMagic = faMagic
  public faEye = faEye
  public faRare = faRegistered

  public imgSrc: string
  @Input() id: number = 1
  @Input() name: string = "Bulbasaur"
  @Input() shiny: boolean = false
  @Input() rare: boolean = true
  @Input() spotted: boolean = true
  async ngOnInit() {
    this.imgSrc = await this.pokeApi.getPokemonImageLink(this.shiny, true, this.id)
  }

}
