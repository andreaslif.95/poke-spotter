import { Component, OnChanges, Input, SimpleChange } from '@angular/core';
import { PokeapiService } from '../../../services/pokeapi/pokeapi.service'

@Component({
  selector: 'app-pokeinfo',
  templateUrl: './pokeinfo.component.html',
  styleUrls: ['./pokeinfo.component.css']
})
export class PokeinfoComponent implements OnChanges {

  constructor(private pokeApi: PokeapiService) { }

  @Input() gender: string;
  @Input() shiny: boolean;
  @Input() pokemon: string;
  
  public pokemonData = null
  public pokeImage = null
  /**
   * Here we subscribe to the changes of our input values.
   * When we recieve a specific pokemon we get it's info from our pokeApi
   * We then retrieve the image.
   * @param changes 
   */
  ngOnChanges(changes: {[propertyName: string]: SimpleChange}): void {
    const gender = changes['gender'] ? changes['gender'].currentValue : this.gender
    const shiny = changes['shiny'] ? changes['shiny'].currentValue : this.shiny
    const pokemon = changes['pokemon'] ? changes['pokemon'].currentValue : null

    if(pokemon !== null) {
      this.pokeApi.getPokemonFromName(pokemon.name.toLowerCase()).then((pokemonData) => {
        this.pokemonData = pokemonData
        this.setImg(gender, shiny, pokemonData)
      })
    }
    else {
      this.setImg(gender, shiny, this.pokemonData)
    }
  }


  async setImg(gender : string, shiny: boolean, pokeData: any) {
    if(pokeData == null) return
    this.pokeImage = await this.pokeApi.getPokemonImageLink(shiny, gender === 'm', pokeData.id)
  }
}
