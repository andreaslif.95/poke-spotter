import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-pokeoptions',
  templateUrl: './pokeoptions.component.html',
  styleUrls: ['./pokeoptions.component.css']
})
/**
 * Component holding input fields, uses eventemitter to transmit the changes to the parent.
 */
export class PokeoptionsComponent implements OnInit {

  constructor() { }

  @Output() changeShiny = new EventEmitter()
  @Output() changeGender = new EventEmitter()
  public shinyV = false
  ngOnInit(): void {
  }

  setShiny(e) {
    this.shinyV = !this.shinyV
    this.changeShiny.emit(this.shinyV)
  }

  setGender(e) {
    this.changeGender.emit(e.target.value)
  }
}
