import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-poketype',
  templateUrl: './poketype.component.html',
  styleUrls: ['./poketype.component.css']
})
/**
 * Used to generate fitting pokemon type badges for the pokemons.
 */
export class PoketypeComponent implements OnInit {

  constructor() { }

  @Input() type : string
  public color = null
  
  ngOnInit(): void {
    for(let i = 0; i < this.data.length; i++) {
      if(this.data[i].name.toLowerCase() === this.type.toLowerCase()) {
        this.color = this.data[i].color
        break
      }
    }

  }



  data = [
    {
        "name": "Normal",
        "color": "#A8A878"
    },
    {
        "name": "Fire",
        "color": "#F08030"
    },
    {
        "name": "Fighting",
        "color": "#C03028"
    },
    {
        "name": "Water",
        "color": "#6890F0"
    },
    {
        "name": "Flying",
        "color": "#A890F0"
    },
    {
        "name": "Grass",
        "color": "#78C850"
    },
    {
        "name": "Poison",
        "color": "#A040A0"
    },
    {
        "name": "Electric",
        "color": "#F8D030"
    },
    {
        "name": "Ground",
        "color": "#E0C068"
    },
    {
        "name": "Psychic",
        "color": "#F85888"
    },
    {
        "name": "Rock",
        "color": "#B8A038"
    },
    {
        "name": "Ice",
        "color": "#98D8D8"
    },
    {
        "name": "Bug",
        "color": "#A8B820"
    },
    {
        "name": "Dragon",
        "color": "#7038F8"
    },
    {
        "name": "Ghost",
        "color": "#705898"
    },
    {
        "name": "Dark",
        "color": "#705848"
    },
    {
        "name": "Steel",
        "color": "#B8B8D0"
    },
    {
        "name": "Fairy",
        "color": "#EE99AC"
    }
]

}
