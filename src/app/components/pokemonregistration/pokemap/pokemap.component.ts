import { Component, AfterViewInit, Input, OnChanges, SimpleChange, Output, EventEmitter} from '@angular/core';
import {PokemapService} from '../../../services/pokemap/pokemap.service'
import { PokeapiService } from '../../../services/pokeapi/pokeapi.service'


@Component({
  selector: 'app-pokemap',
  templateUrl: './pokemap.component.html',
  styleUrls: ['./pokemap.component.css']
})
/**
 * This component is used to render and manipulate our map.
 */
export class PokemapComponent implements AfterViewInit, OnChanges {

  @Input() pokemon: any;
  @Input() gender: string;
  @Input() shiny: boolean;

  @Output() latLng = new EventEmitter();

  private map;
  public iconOnClick = false
  public lat; 
  public lng

  constructor(private pokeMap: PokemapService, private pokeApi: PokeapiService) { }
  /**
   * We track hanges to our input in order to actually change the pointer used.
   * @param changes 
   */
  ngOnChanges(changes: {[propertyName: string]: SimpleChange}): void {
    const pokemon = changes['pokemon'] ? changes['pokemon'].currentValue : this.pokemon
    if(pokemon != null) {
      this.pokeApi.getPokemonImageLink(this.shiny, this.gender === 'm', pokemon.id).then(url => {
        this.pokeMap.setPokemonMapMarker(url, this.map, () => this.addMarker())
      })
      
    }
  }
  /**
   * Creates the map after our map div has been rendered.
   */
  ngAfterViewInit(): void {
    this.map = this.pokeMap.createMap((e) => this.onMapClick(e))
  }

  onMapClick(e) {
    if(this.iconOnClick) {
      this.lat = e.latlng.lat
      this.lng = e.latlng.lng
      this.addMarker()
    }
  }

  addMarker() {
    this.latLng.emit({lat: this.lat, lng: this.lng})
    this.pokeMap.addMarker(this.map, this.lat, this.lng)
  }


}
