import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PokedropdownComponent } from './pokedropdown.component';

describe('PokedropdownComponent', () => {
  let component: PokedropdownComponent;
  let fixture: ComponentFixture<PokedropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PokedropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PokedropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
