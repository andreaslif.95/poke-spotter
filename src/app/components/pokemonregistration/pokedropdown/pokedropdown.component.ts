import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import score from 'string-score'
import {Observable} from 'rxjs';

import {debounceTime, map} from 'rxjs/operators';

@Component({
  selector: 'app-pokedropdown',
  templateUrl: './pokedropdown.component.html',
  styleUrls: ['./pokedropdown.component.css'],
  inputs: ['error', 'pokes']
})

export class PokedropdownComponent implements OnInit {
  //Emits event to the parents.
  @Output() loadData = new EventEmitter<string>();
  @Output() selectPoke = new EventEmitter<string>();
  constructor() { }

  public pokes = []
  public error = true

  ngOnInit() {
    this.emitLoadData()
  }

  emitLoadData() {
    this.loadData.emit()
  }
  
  emitSelectPoke(pokemon: any) {
    this.selectPoke.emit(JSON.stringify(pokemon))
  }
  /**
   * This function uses the input to filter all pokemon names to find the ones with a good score.
   * Will only pick pokemon names that includes the search word and are longer than the search word.
   * Will sort by the heights string score. eg The one that is most equal is shown in the top.
   */
  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(20),
      map(name => name === '' ? []
        : this.pokes.filter(poke => poke.name.toLowerCase().includes(name.toLowerCase()) && poke.name.length >= name.length)
        .sort((pokeA: any,pokeB: any) => score(name.toLowerCase(), pokeB.name.toLowerCase(), 0.5) - score(name.toLowerCase(), pokeA.name.toLowerCase(), 0.5)))
    )
  /**
   * Used to show the name of the pokemon when we select it from the suggestions.
   * @param x 
   */
  formatter = (pokemon: any) => {
    this.emitSelectPoke(pokemon)
    return pokemon.name
  }
}