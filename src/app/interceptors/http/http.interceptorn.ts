import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import {SessionService} from '../../services/session/session.service'

@Injectable()
export class HttpInterceptorn implements HttpInterceptor {

  constructor(private session : SessionService) {}
  //Applies our token to the headers.
  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    let token = this.session.get() || ''
    request = request.clone({ headers: request.headers.set('token',token ) });

    return next.handle(request);
  }
}
