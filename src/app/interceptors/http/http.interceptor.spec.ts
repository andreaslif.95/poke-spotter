import { TestBed } from '@angular/core/testing';

import { HttpInterceptorn } from './http.interceptorn';

describe('HttpInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      HttpInterceptorn
      ]
  }));

  it('should be created', () => {
    const interceptor: HttpInterceptorn = TestBed.inject(HttpInterceptorn);
    expect(interceptor).toBeTruthy();
  });
});
