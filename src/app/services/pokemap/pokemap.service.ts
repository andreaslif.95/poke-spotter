import { Injectable } from '@angular/core';
import * as L from 'leaflet'
@Injectable({
  providedIn: 'root'
})
export class PokemapService {

  constructor() { }

  setPokemonMapMarker(url: string, map: any, updateMarkerFunc : Function) {
      map.icon = L.icon({
        iconUrl: url,

        iconSize:     [80,80], // size of the icon

      })
    if(map.marked != null) {
      updateMarkerFunc()
    }
  }

  createMap(onClickFunc: Function) : any {
    const map = L.map('map', {
      center: [ 39.8282, -98.5795],
      zoom: 3
    })
    
    const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 19,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });

    tiles.addTo(map);
    map.on('click', onClickFunc)
    return map
  }

  addMarker(map : any, lat: number, lng: number) {
    const marked = L.marker([ lat, lng], {icon: map.icon})
    if(map.marked != null) {
      map.removeLayer(map.marked)
    }
    map.panTo({lat: lat, lng: lng})
    map.marked = marked
    map.addLayer(marked)

  }
}
