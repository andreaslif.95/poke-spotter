import { TestBed } from '@angular/core/testing';

import { PokemapService } from './pokemap.service';

describe('PokemapService', () => {
  let service: PokemapService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PokemapService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
