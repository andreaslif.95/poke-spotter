import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { map, tap, catchError } from 'rxjs/operators'
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
/**
 * Used to recieve and filter data recieved from our backend.
 */
export class BackapiService {

  constructor(private http: HttpClient) { }
  /**
   * 
   * @param id - id of the pokemon
   * @param name  - name of the pokemo
   * @param coordinates - where the pokemon was found
   * @param gender  - Gender of the pokemon
   * @param shiny - If it was shiny or not.
   */
  registerPoke(id: number, name: string, coordinates: any, gender: string, shiny: boolean) : Promise<any>{
    console.log(id)
    const body = {
      "spotted": {
        id,
        name,
        latitude: coordinates.lat,
        longitude: coordinates.lng,
        gender: gender == 'm' ? 'male' : 'female',
        shiny,
        date: new Date()
      }
    }
    return this.http.put('https://experis-pokemon-api.herokuapp.com/users/', body).toPromise()
  }
  /**
   * Used to register a speicific user.
   * @param username 
   * @param password 
   */
  registerUser(username: string, password: string) : Promise<any> {
    const body = {
      username,
      password,
      spotted: []
    }
    return this.http.post('https://experis-pokemon-api.herokuapp.com/users', body, {
      headers: new HttpHeaders({
        'content-type': 'application/json'
      })
    })
    .pipe(
      catchError(err => {
        if(err.status === 409) {
          return throwError(0)
        }
        return throwError(1)
      })
    )
    .toPromise()
  }
  /**
   * Get the current users info and extracts the spotted pokemon.
   */
  getSpottedPokemons() : Promise<any> {
    return this.http.get('https://experis-pokemon-api.herokuapp.com/users?username=n')
    .pipe(
      tap(resp => console.log(resp)),
      map((resp : any) => {
        const spottedData = {}
        resp[0].spotted.forEach(pokemon => {
          if(spottedData[pokemon.name] === undefined) {
            spottedData[pokemon.name] = {
              count: 0,
              shiny: false,
              male: false,
              female: false
            }
          }
          const poke = spottedData[pokemon.name]

          poke.count ++
          if(pokemon.shiny) {
            poke.shiny = true
          }
          if(pokemon.gender === 'male') {
            poke.male = true
          }
          if(pokemon.gender === 'female') {
            poke.female = true
          }
        })
        return spottedData
      }),
      tap((p) => console.log(p))
    )
    .toPromise()

  }
  /**
   * Gets the rarest pokemons.
   */
  getRarestPokemons() : Promise<any> {
    return this.http.get('https://experis-pokemon-api.herokuapp.com/rarest-pokemons')
    .pipe(
      tap(resp => console.log(resp)),
      map((resp: any) => resp.map((rare => rare.id)))
    )
    .toPromise()
  }
  /**
   * Gets the ranking list, manipulates the data to store more information such as.
   * How many shines the user has seen, uniques and total spotted.
   */
  getRankingList() : Promise<any> {
    return this.http.get('https://experis-pokemon-api.herokuapp.com/users/toprank')
    .pipe(
      map((resp: Array<any>) => {
        resp.forEach(user => {
          user.spottedTotal = user.spotted.length
          user.uniques = 0
          user.shiny = 0
          let pokes = {}
          user.spotted.forEach(poke => {
            if(pokes[poke.name] === undefined) {
              user.uniques++;
              pokes[poke.name] = {}
            }
            if(poke.shiny) {
              user.shiny++
            }
          });

        })
        return resp
      })
    )
    .toPromise()
  }
  /**
   * Logs a user in.
   * @param username 
   * @param password 
   */
  login(username: string, password: string) {
    return this.http.post('https://experis-pokemon-api.herokuapp.com/login', {username, password})
    .toPromise()
  }
}
