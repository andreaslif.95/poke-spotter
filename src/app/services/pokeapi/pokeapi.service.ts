import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { map, tap } from 'rxjs/operators'


@Injectable({
  providedIn: 'root'
})
export class PokeapiService {

  constructor(private http: HttpClient) { }

  /**
   * Get all pokemons from the pokemon api, manipulates the data to get the id aswell.
   */
  getAllPokemonNameAndId() : Promise<any> {
    return this.http.get('https://pokeapi.co/api/v2/pokemon?limit=151&offset=0')
    .pipe(
      map((resp: any) => {
        const result = resp.results
        return result.map(pokemon => this.capitalize(pokemon.name))
      }),
      map((pokes: any) => {
        return pokes.map((pokeName, index) => {
          return {
            name : pokeName,
            id: index +1
          }
        })
      }),
    ).toPromise()
  }
  /**
   * Gets a specific pokemons data by it's name
   * @param name 
   */
  getPokemonFromName(name: string) : Promise<any> {
    return this.http.get('https://pokeapi.co/api/v2/pokemon/'+name)
    .pipe(
      map((pokemon: any) => {
        return {
          name: pokemon.name,
          id: pokemon.id,
          types: pokemon.types
        }
      })
    )
    .toPromise()
  }
  capitalize(s) : String{
    return s.charAt(0).toUpperCase() + s.slice(1)
  }
  /**
   * Get s a specific pokemons image link.
   * Checks so that the image works.
   * @param shiny 
   * @param male 
   * @param id 
   */
  getPokemonImageLink(shiny: boolean, male: boolean, id: number) : Promise<string> {
    return new Promise((resolve, reject) => {
      const url = 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon' + (shiny ? '/shiny' : '') +'' + (!male ? '/female' : '' )+ '/' + id + '.png'

      const img = new Image()

      img.onload = () => {
        resolve(url)
      }

      img.onerror = () => {
        resolve( 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon' + (shiny ? '/shiny' : '') + '/' + id + '.png')
      }
      img.src = url
    })
  }
}
