import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, tap } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class GitlabService {

  constructor(private http: HttpClient) { }

  getUserFromAccessToken(token: string) : Promise<any>{
	  	
        return this.http.get('https://gitlab.com/api/v4/user', {
          headers: new HttpHeaders({
            Authorization: `Bearer ${token}`
          })
        })
        .pipe(
          map((resp: any) => resp.username)
        )
        .toPromise()
      
  }

  getTokenFromGitlab() : void {
    const config = {
      redirect_uri: 'http://localhost:4200/data',
      client_id: '73940e15c21076f6ad121501cd61d937d03cc83bcf2542a0f806e809c4a6f012',
      scope: 'read_user openid profile email',
      response_type: 'token'
      }
      location.href = `https://gitlab.com/oauth/authorize?${new URLSearchParams(config).toString()}`;
  }
}
