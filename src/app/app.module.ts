import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { AppRoutingModule } from './app-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule, HTTP_INTERCEPTORS  } from '@angular/common/http';

import { AppComponent } from './app.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterUserComponent } from './views/register-user/register-user.component';
import { PokemonregistrationComponent } from './views/pokemonregistration/pokemonregistration.component';
import { PokedropdownComponent } from './components/pokemonregistration/pokedropdown/pokedropdown.component';
import { PokeinfoComponent } from './components/pokemonregistration/pokeinfo/pokeinfo.component';
import { PokemapComponent } from './components/pokemonregistration/pokemap/pokemap.component';
import { PokeoptionsComponent } from './components/pokemonregistration/pokeoptions/pokeoptions.component';
import { PoketypeComponent } from './components/pokemonregistration/poketype/poketype.component';
import { PokemonListItemComponent } from './components/pokemon/pokemon-list-item/pokemon-list-item.component';
import { PokemonComponent } from './views/pokemon/pokemon.component';
import { PokemonListComponent } from './components/pokemon/pokemon-list/pokemon-list.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { RankingListComponent } from './components/ranking/ranking-list/ranking-list.component';
import { RankingsComponent } from './views/rankings/rankings.component';

import {HttpInterceptorn} from './interceptors/http/http.interceptorn';
import { NavbarComponent } from './components/navbar/navbar.component'


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterUserComponent,
    PokemonregistrationComponent,
    PokedropdownComponent,
    PokeinfoComponent,
    PokemapComponent,
    PokeoptionsComponent,
    PoketypeComponent,
    PokemonListItemComponent,
    PokemonComponent,
    PokemonListComponent,
    RankingListComponent,
    RankingsComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormsModule,
    FontAwesomeModule,
    HttpClientModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorn, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
