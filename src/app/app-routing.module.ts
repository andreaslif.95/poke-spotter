import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './views/login/login.component';
import { RegisterUserComponent } from './views/register-user/register-user.component';
import { PokemonregistrationComponent } from './views/pokemonregistration/pokemonregistration.component'
import { PokemonComponent } from './views/pokemon/pokemon.component'
import {RankingsComponent} from './views/rankings/rankings.component'
import {GitlabGuard} from './guards/GitLab/gitlab.guard'
import {AuthGuard} from './guards/Auth/auth.guard'

const routes: Routes = [
	{
		path: 'login',
    component: LoginComponent, 
	}, 
	{
		path: 'register-user',
		component: RegisterUserComponent
	},
	{
		path: 'data',
    component: RegisterUserComponent,
    canActivate: [GitlabGuard]
	},
	{
    path: 'regpokemon',
    component: PokemonregistrationComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'pokemons',
    component: PokemonComponent
  },
  {
    path: 'rankings',
    component: RankingsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
