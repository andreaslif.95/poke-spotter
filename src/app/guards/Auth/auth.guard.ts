import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { SessionService } from '../..//services/session/session.service'
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
/**
 * Used to navigate away from pages not allowed to visit when not authenticated.
 */
export class AuthGuard implements CanActivate {
    constructor(private router: Router, private session: SessionService) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if(!this.session.get()) {
        this.router.navigateByUrl('/pokemons')     
        return false
      }
    return true;
  }
  
}
