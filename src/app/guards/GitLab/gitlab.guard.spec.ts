import { TestBed } from '@angular/core/testing';

import { GitlabGuard } from './gitlab.guard';

describe('GitlabGuard', () => {
  let guard: GitlabGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(GitlabGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
