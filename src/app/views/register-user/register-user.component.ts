import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router, RouterModule } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {GitlabService} from '../../services/gitlab/gitlab.service'
import {BackapiService} from '../../services/backapi/backapi.service'
import { SessionService } from 'src/app/services/session/session.service';

@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.css']
})
export class RegisterUserComponent implements OnInit {

	public error : number
	registerUserForm: FormGroup = new FormGroup({
  	username: new FormControl(''),
  	password: new FormControl('')

  	})

  	constructor(private http: HttpClient, private router: Router, private gitlab: GitlabService, private backApi: BackapiService, private session: SessionService) {
	if (this.session.get() !== false) {
		this.router.navigateByUrl('/pokemons')
		}
	}

 	async ngOnInit() {
		localStorage.setItem('current_page', 'register-user')
	  	if(location.hash) {
	  		let fragment = new URLSearchParams(location.hash.substring(1));
			this.username = await this.gitlab.getUserFromAccessToken(fragment.get("access_token"))	  	
	  	}
 	}

  	get username() {
  		return this.registerUserForm.get('username');
  	}
	set username(user_name){
		this.registerUserForm.setValue({username: user_name, password: ""});
	}
  	get password() {
  		return this.registerUserForm.get('password');
  	}

	gettoken() {
		this.gitlab.getTokenFromGitlab()
	}
	//Saves the user, eg. we register the user.
	//After registering we logins the user.
	async saveuser() {
		this.error = 0
		try {
			await this.backApi.registerUser(this.username.value, this.password.value)
			const token : any = await this.backApi.login(this.username.value, this.password.value)
			this.session.save(token.token)
			this.router.navigateByUrl('/pokemons');
		}
		catch(err) {
			this.error = err
		}


	}

}
