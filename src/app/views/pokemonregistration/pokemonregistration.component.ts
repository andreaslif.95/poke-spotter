import { Component, OnInit } from '@angular/core';
import {PokeapiService} from '../../services/pokeapi/pokeapi.service'
import {BackapiService} from '../../services/backapi/backapi.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-pokemonregistration',
  templateUrl: './pokemonregistration.component.html',
  styleUrls: ['./pokemonregistration.component.css'],
})
export class PokemonregistrationComponent implements OnInit {

  constructor(
    private pokeApi: PokeapiService,
    private backApi: BackapiService,
    private route: Router) { }

  ngOnInit(): void {
  }

  
  public pokes = []
  public error = false
  public selectedPokemon = null
  public shiny = false
  public gender = 'm'
  public coordinates = null
  /**
   * Loads data from the pokemon api.
   */
  async loadData() {
    try {
      this.pokes = await this.pokeApi.getAllPokemonNameAndId() 
      this.error = false
    }
    catch(err) {
      this.error = true
    }
  }
  //Returned as a stringyfied json object
  selectPokemon(pokemon: string) {
    this.selectedPokemon = JSON.parse(pokemon)
  }

  setShiny(value) {
    this.shiny = value
  }

  setGender(gender) {
    this.gender = gender.toLowerCase()
  }

  setCoordinates(coordinates: any) {
    this.coordinates = coordinates
  }
  /**
   * Takes all values fetched from the events and tries to register.
   */
  async onRegisterClick() {
    try {
      await this.backApi.registerPoke(this.selectedPokemon.id, this.selectedPokemon.name, this.coordinates, this.gender, this.shiny)
      await confirm('Sucessfully registered the pokemon!')
      this.route.navigateByUrl('/pokemons')
    }
    catch(err) {
      await confirm('Something went wrong, try again.')
    }

  }
}
