import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PokemonregistrationComponent } from './pokemonregistration.component';

describe('PokemonregistrationComponent', () => {
  let component: PokemonregistrationComponent;
  let fixture: ComponentFixture<PokemonregistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PokemonregistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PokemonregistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
