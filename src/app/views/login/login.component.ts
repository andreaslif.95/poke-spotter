import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GitlabService} from '../../services/gitlab/gitlab.service'
import { SessionService } from 'src/app/services/session/session.service';
import { Router } from '@angular/router';
import { BackapiService } from 'src/app/services/backapi/backapi.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup = new FormGroup({
  	username: new FormControl(''),
  	password: new FormControl('')
  })

  public loginError: string = '';

  constructor(private http: HttpClient, 
    private session: SessionService, 
    private router: Router, 
    private gitlab: GitlabService,
    private backApi : BackapiService
    ) {
    if (this.session.get() !== false) {
      this.router.navigateByUrl('/pokemons')
    }

  }

  async ngOnInit() {
    //Sets the current page to login to get a good redirect.
    localStorage.setItem('current_page', 'login')
    if(location.hash) {
      //Extracts the acces_token from th eurl.
      let fragment = new URLSearchParams(location.hash.substring(1));
      this.loginForm.setValue({
        username : await this.gitlab.getUserFromAccessToken(fragment.get("access_token")),
        password: this.loginForm.value.password
      })
    }
  }


  get username() {
    return this.loginForm.get('username')
  }
  get password() {
    return this.loginForm.get('password')
  }

  
	gettoken() {
		this.gitlab.getTokenFromGitlab()
  }
  
  async onLoginClicked()  {

    this.loginError = "";

    try {
      const token : any = await this.backApi.login(this.username.value, this.password.value)
      this.session.save(token.token)
      this.router.navigateByUrl('/pokemons')
    } catch (e) {
      this.loginError = e.message
    }

  }
}